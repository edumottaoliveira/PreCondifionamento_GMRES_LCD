#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <stdio.h>

#define FREE(ptr) {	if (ptr != NULL) {free(ptr); ptr = NULL;} }
#define ERROR(msg) { printf("\nERRO: %s\n", msg); exit(1); }
#define CHECKALLOC(p) { if (p == NULL) ERROR("Memoria nao alocada."); }


#endif
