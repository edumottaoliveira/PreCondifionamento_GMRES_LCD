#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "./CommonFiles/protos.h"
#include "csrMatrixSystem.h"
#include "solvers.h"

double get_time ()
{
	struct timeval tv; gettimeofday(&tv, 0);
	return (double)(tv.tv_sec * 100.0 + tv.tv_usec / 10000.0);
}

void precondicionamentoILU(MAT* A, MAT* L, MAT* U, int p) {

	/*---------------------------------------------*/
	/*---COMO USAR O ALGORITMO ILUP----------------*/
	/*---------------------------------------------*/
	double time;

	SparMAT* mat = (SparMAT*) malloc(sizeof(SparMAT));				// Alocando estruturas para o ILU(p)
	SparILU* lu  = (SparILU*) malloc(sizeof(SparILU));

	printf("\n  [ CALCULANDO PRECONDICIONADOR ILU ]\n");
	/*---START TIME---------------> */ time = get_time();
	CSRto_SPARMAT (A,mat);								// Convertendo CSR para estrutura especial
	ILUP          (mat,lu,p);							// Algoritmo ILU(p)
	SPARILU_toCSR (lu,L,U);								// Convertendo estrutura especial para CSR
	/*---FINAL TIME---------------> */ time = (get_time() - time)/100.0;
	printf("  - Tempo total              : %.6f sec\n", time);

	SPARILU_clean (lu);								// Liberando memória da estrutura lu
	SPARMAT_clean (mat);								// Liberando memória da estrutura mat

	/* L contém a parte estritamente inferior de M / L->D contém a diagonal = 1.0 */
	/* U contém a parte estritamente superior de M / U->D contém a diagonal       */
	//MATRIX_printLU (A,L,U);

}


void reordenamentoRCM(MAT* A, int* p) {
	/*---------------------------------------------*/
	/*---COMO USAR O REORDENAMENTO RCM-------------*/
	/*---------------------------------------------*/
	double time;
	int  bandwidth;

	bandwidth = (int) MATRIX_bandwidth(A);					// Calcula Largura de Banda da matriz original
	printf("\n  [ REORDENANDO com RCM ]\n");
	printf("  - Largura de Banda inicial : %d\n", bandwidth);

	/*---START TIME---------------> */ time = get_time();
	REORDERING_RCM_opt(A,&p);						// Aplica o reordenamento RCM na matriz A
	MATRIX_permutation(A,p); 						// Aplica a permutação em A para trocar linhas e colunas
	/*---FINAL TIME---------------> */ time = (get_time() - time)/100.0;

	bandwidth = (int) MATRIX_bandwidth(A);					// Calcula Largura de Banda da matriz reordenada
	printf("  - Largura de Banda final   : %d\n", bandwidth);
	printf("  - Tempo total              : %.6f sec\n\n", time);



	//for (int i = 0; i < A->n; i++)
	//		printf("%d\n", p[i]);
}



int main (int argc, char* argv[])
{
	if (argc != 2)
	{
		printf("\n Erro! Sem arquivo da matriz (.mtx)"); 
		printf("\n Modo de usar: ./program <nome_da_matriz> Saindo... [main]\n\n");
		return 0;
	}
	
	int* p = NULL;												// Vetor de permutação
	MAT* A = (MAT*) malloc (sizeof(MAT));
	MATRIX_readCSR (A,argv[1]);
	//reordenamentoRCM(A, p);

 	//MAT* preL = (MAT*) malloc(sizeof(MAT));						// Alocando matriz L
	//MAT* preU = (MAT*) malloc(sizeof(MAT));						// Alocando matriz U
	//precondicionamentoILU(A, preL, preU, 0);

	LinearSystem* ls = criarSistemaComSolUnitaria(A);

	int strLen = strlen(argv[1]);
	argv[1][strLen - 4] = '\0';
	printf("Input: %s\n", argv[1]);
	double tol = 0.00000001;

	//GMRES_completo(ls, 50, 500000, tol, preL, preU, argv[1], 0, ILU);
	//GMRES_completo(ls, 50, 500000, tol, NULL, NULL, argv[1], 0, SEIDEL);

	//LCD_completo(ls, 20, 50000, tol, NULL, NULL, argv[1], 0, SEIDEL);
	//LCD_completo(ls, 20, 50000, tol, preL, preU, argv[1], 0, ILU);

	for (int i = 0; i < 5; i++) {
		printf("\t%0.16lf\n", ls->x[i]);
	}
	freeLinearSystem(ls);

	free(p);
	//MATRIX_clean(preL);
	//MATRIX_clean(preU);

	return 0;
}





