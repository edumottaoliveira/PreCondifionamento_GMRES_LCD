#ifndef SOLVERS_H
#define SOLVERS_H

#include "csrMatrixSystem.h"
enum Precondicionamento {ILU, SEIDEL, SEM_PRECOND};

void GMRES(LinearSystem* ls, int iMax, double tolerancia);

void GMRES_Restart(LinearSystem* ls, int iMax, int lMax, double tolerancia, char* nomeMatriz);

void GMRES_completo(LinearSystem* ls, int iMax, int lMax, double tolerancia, MAT* preL, MAT* preU,char* nomeMatriz, int reordenamento, int precondicionamento);

void LCD(LinearSystem* ls, int kMax, double tolerancia);

void LCD_Restart(LinearSystem* ls, int kMax, int lMax, double tolerancia, char* nomeMatriz);

void LCD_completo(LinearSystem* ls, int kMax, int lMax, double tolerancia, MAT* preL, MAT* preU,char* nomeMatriz, int reordenamento, int precondicionamento);

#endif
