#include <math.h>
#include "vector.h"
#include "util.h"

double* alocarVetor(int n) {
	double* v = (double*) calloc(n, sizeof(double));
	return v;
}

void freeVetor(double* v) {
	FREE(v);
}

void printVetor(double* v, int n, char* nome) {
	printf("\n%s = \n\n", nome);
	for (int j = 0; j < n; j++) {
		printf("\t%0.16lf\n", v[j]);
	}
}

double* copiarVetor(double* v, int n) {
	double* copia = alocarVetor(n);
	for (int i = 0; i < n; i++) {
		copia[i] = v[i];
	}
	return copia;
}

void copiarValores(double* vetOrigem, double* vetDestino, int n) {
	for (int i = 0; i < n; i++) {
		vetDestino[i] = vetOrigem[i];
	}
}

double normaEuclidiana(double* v, int n) {
	double soma = 0.0;
	for (int i = 0; i < n; i++) {
		soma += pow(v[i], 2);
	}
	return sqrt(soma);
}

double normaEuclidianaAoQuadrado(double* v, int n) {
	double soma = 0.0;
	for (int i = 0; i < n; i++) {
		soma += pow(v[i], 2);
	}
	return soma;
}

void tocarPonteirosVetores(double* v1, double* v2) {
	double* temp = v1;
	v1 = v2;
	v2 = temp;
}

double normaInfinito(double* v, int n) {
	double max = 0.0;
	for (int i = 0; i < n; i++) {
		if (fabs(v[i]) > max) {
			max = fabs(v[i]);
		}
	}
	return max;
}

void dividirVetorPorConstante(double* v, double constante, int n) {
	for (int i = 0; i < n; i++) {
		v[i] /= constante;
	}
}

double maxSubtracaoVetVet(double* v1, double* v2, int n) {
	double max = 0.0;
	double absDiferenca;
	for (int i = 0; i < n; i++) {
		absDiferenca = fabs(v1[i] - v2[i]);
		if (absDiferenca > max) {
			max = absDiferenca;
		}
	}
	return max;
}

void zerarVetor(double* v, int n) {
	for (int i = 0; i < n; i++) {
		v[i] = 0.0;
	}
}

double* gerarVetorUnitario(int n) {
	double* v = alocarVetor(n);
	for (int i = 0; i < n; i++) {
		v[i] = 1;
	}
	return v;
}

// v1 . v2
double produtoEscalarVetVet(double* v1, double* v2, int n) {
	double resultado = 0.0;
	for (int i = 0; i < n; i++) {
		resultado +=  v1[i] * v2[i];
	}
	return resultado;
}

// v1 + scalar*v2
void somarComProdutoEscalarVet (double* v1, double escalar, double* v2, int n) {
	for (int i = 0; i < n; i++) {
		v1[i] += escalar * v2[i];
	}
}

void somarVetComVet(double* v1, double* v2, double* resultado, int n) {
	for (int i = 0; i < n; i++) {
		resultado[i] = v1[i] + v2[i];
	}
}

void subtrairVetDeVet(double* v1, double* v2, double* resultado, int n) {
	for (int i = 0; i < n; i++) {
		resultado[i] = v1[i] - v2[i];
	}
}

void produtoVetorPorConstante(double* v, double constante, double* resultado, int n) {
	for (int i = 0; i < n; i++) {
		resultado[i] = v[i] * constante;
	}
}



