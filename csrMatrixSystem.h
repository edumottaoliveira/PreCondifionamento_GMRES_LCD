#ifndef CSR_MATRIX_SYSTEM_H
#define CSR_MATRIX_SYSTEM_H

#include "./CommonFiles/protos.h"
#include "vector.h"

typedef struct {
	MAT* A;
	double* x;
	double* b;
} LinearSystem;

LinearSystem* criarSistemaComSolUnitaria(MAT* A);

void freeLinearSystem(LinearSystem *ls);

void printLinearSystem(LinearSystem *ls);

// A x v
void produtoMatVet(MAT* A, double* v, double* resultado);

void resolverSistemaL(MAT* L, double* x, double* b);

void resolverSistemaU(MAT* U, double* x, double* b);

void resolverSistemaTriangularInferior(MAT* L, double* x, double* b);

void resolverSistemaTriangularSuperior(MAT* U, double* x, double* b);

#endif
