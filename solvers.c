#include "solvers.h"
#include "util.h"
#include <math.h>
#include <time.h>

/*----------------------------------------------------------------------------
 * GMRES
 *--------------------------------------------------------------------------*/


void GMRES_completo(LinearSystem* ls, int iMax, int lMax, double tolerancia, MAT* preL, MAT* preU,char* nomeMatriz, int reordenamento, int precondicionamento) {
	clock_t start = clock();
	char nomeArquivo[100];

	switch (precondicionamento) {
	case ILU:
		if (reordenamento == 1)
			sprintf(nomeArquivo, "./plot/%s_gmres_%d_ILU_RCM.dat", nomeMatriz, iMax);
		else
			sprintf(nomeArquivo, "./plot/%s_gmres_%d_ILU.dat", nomeMatriz, iMax);
		break;
	case SEIDEL:
		if (reordenamento == 1)
			sprintf(nomeArquivo, "./plot/%s_gmres_%d_Seidel_RCM.dat", nomeMatriz, iMax);
		else
			sprintf(nomeArquivo, "./plot/%s_gmres_%d_Seidel.dat", nomeMatriz, iMax);
		break;
	case SEM_PRECOND:
		if (reordenamento == 1)
			sprintf(nomeArquivo, "./plot/%s_gmres_%d_RCM.dat", nomeMatriz, iMax);
		else
			sprintf(nomeArquivo, "./plot/%s_gmres_%d.dat", nomeMatriz, iMax);
		break;
	}

	printf("salvando em: %s\n", nomeArquivo);
	FILE* arquivo = fopen(nomeArquivo, "w");
	if (arquivo == NULL) {
		ERROR("Nao foi possivel criar arquivo de saida.");
		exit(0);
	}

	printf("Resolvendo pelo GMRES...\n\n");
	MAT* A = ls->A;
	int n = A->n;
	double* x = ls->x;
	double* b = ls->b;
	// U(n x iMax+1), um vetor de tam n para cada interacao +1
	double** U = (double**) malloc( (iMax + 1) * sizeof(double*));
	for (int k = 0; k < iMax + 1; k++) {
		U[k] = alocarVetor(n);
	}
	// H(iMax x iMax+1), um vetor de tam iMax+1 para cada iteracao
	double** H = (double**) malloc( iMax * sizeof(double*));
	for (int k = 0; k < iMax; k++) {
		H[k] = alocarVetor(iMax+1);
	}

	double* y = alocarVetor(iMax);
	double* c = alocarVetor(iMax);
	double* s = alocarVetor(iMax);
	double* e = alocarVetor(iMax+1);
	double* p = alocarVetor(n);
	double* q = alocarVetor(n);
	double* Ax = alocarVetor(n);
	double r, rho, limiteRho = tolerancia * normaEuclidiana(b, n);
	int i, l = 0, numTotalIter = 0;

	MATRIX_print(A, "A");
	do {
			produtoMatVet(A, x, Ax);
			subtrairVetDeVet(b, Ax, p, n); //b - Ax = p

			switch (precondicionamento) {
			case ILU:		// LU*u[0] = b-Ax -> U*u[0] = q   e   b-Ax = p ->  L*q = p
				resolverSistemaL(preL, q, p);
				resolverSistemaU(preU, U[0], q);
				break;
			case SEIDEL:	// (L+D)(U+D)*u[0] = b-Ax -> (U+D)*u[0] = q   e   b-Ax = p -> (L+D)*q = p
				resolverSistemaTriangularInferior(A, q, p);
				resolverSistemaTriangularSuperior(A, U[0], q);
				break;
			case SEM_PRECOND:
				copiarValores(p, U[0], n);
				break;
			}

			e[0] = normaEuclidiana(U[0], n);
			dividirVetorPorConstante(U[0], e[0], n);
			rho = e[0];
			i = 0;
			while (rho > limiteRho && i < iMax) {

				produtoMatVet(A, U[i], p);

				switch (precondicionamento) {
				case ILU:
					resolverSistemaL(preL, q, p);
					resolverSistemaU(preU, U[i+1], q);
					break;
				case SEIDEL: 	// (L+D)(U+D)*u[i+1] = A*U[i] -> (U+D)*u[i+1] = q  e  A U[i] = p -> (L+D)*q = p
					resolverSistemaTriangularInferior(A, q, p);
					resolverSistemaTriangularSuperior(A, U[i+1], q);
					break;
				case SEM_PRECOND:
					copiarValores(p, U[i+1], n);
					break;
				}

				// Gram - Schmidt
				for (int j = 0; j <= i; j++) {
					H[i][j] = produtoEscalarVetVet(U[i+1], U[j], n); // Iteracao elem j do H atual
					somarComProdutoEscalarVet(U[i+1], -H[i][j], U[j], n);
				}

				H[i][i+1] = normaEuclidiana(U[i+1], n);
				dividirVetorPorConstante(U[i+1], H[i][i+1], n);

				//Algoritmo QR
				for (int j = 0; j < i; j++) {
					double temp = H[i][j];
					H[i][j] =  c[j] * temp + s[j] * H[i][j+1];
					H[i][j+1] = (-s[j]) * temp + c[j] * H[i][j+1];
				}
				r = sqrt( pow(H[i][i], 2) + pow(H[i][i+1], 2));

				c[i] = H[i][i] / r;
				s[i] = H[i][i+1] / r;
				H[i][i] = r;
				H[i][i+1] = 0.0;

				e[i+1] = (-s[i]) * e[i];
				e[i] = c[i] * e[i];

				rho = fabs(e[i+1]);

				printf("rho %d = %0.9lf\n", numTotalIter, rho);
				//if (numTotalIter % 100 == 0) {
					//fprintf(arquivo, "%d\t%0.8lf\t%0.12lf\n", numTotalIter, log(rho), rho);
				//}

				numTotalIter++;
				i++;
			}

			i = i - 1;
			for (int j = i; j >= 0; j--) {
				double sum = 0.0;
				for (int l = j+1; l <= i; l++) {
					 sum += H[l][j] * y[l];
				}
				y[j] = (e[j] - sum) / H[j][j];
			}

			//printf("Computando x ...\n");
			for (int j = 0; j <= i; j++) {
				somarComProdutoEscalarVet(x, y[j], U[j], n);
			}
			l++;
	} while (rho > limiteRho && l < lMax);

	printf("Resolvido em %d iteracoes.\n", numTotalIter);
	// Limpa memoria
	for (int k = 0; k < iMax+1; k++) {
		freeVetor(U[k]);
	}
	FREE(U);
	for (int k = 0; k < iMax; k++) {
		freeVetor(H[k]);
	}
	FREE(H);
	freeVetor(y);
	freeVetor(c);
	freeVetor(s);
	freeVetor(e);
	freeVetor(p);
	freeVetor(q);
	freeVetor(Ax);

	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	fprintf(arquivo, "Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter );
	printf("Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter);
	fclose(arquivo);
}



void LCD_completo(LinearSystem* ls, int kMax, int lMax, double tolerancia, MAT* preL, MAT* preU,char* nomeMatriz, int reordenamento, int precondicionamento) {
	char nomeArquivo[100];

	switch (precondicionamento) {
	case ILU:
		if (reordenamento == 1)
			sprintf(nomeArquivo, "./plot/%s_lcd_%d_ILU_RCM.dat", nomeMatriz, kMax);
		else
			sprintf(nomeArquivo, "./plot/%s_lcd_%d_ILU.dat", nomeMatriz, kMax);
		break;
	case SEIDEL:
		if (reordenamento == 1)
			sprintf(nomeArquivo, "./plot/%s_lcd_%d_Seidel_RCM.dat", nomeMatriz, kMax);
		else
			sprintf(nomeArquivo, "./plot/%s_lcd_%d_Seidel.dat", nomeMatriz, kMax);
		break;
	case SEM_PRECOND:
		if (reordenamento == 1)
			sprintf(nomeArquivo, "./plot/%s_lcd_%d_RCM.dat", nomeMatriz, kMax);
		else
			sprintf(nomeArquivo, "./plot/%s_lcd_%d.dat", nomeMatriz, kMax);
		break;
	}
	printf("salvando em: %s\n", nomeArquivo);
	FILE* arquivo = fopen(nomeArquivo, "w");
	clock_t start = clock();

	printf("Resolvendo pelo LCD...\n\n");
	int n = ls->A->n;
	MAT* A = ls->A;
	double* b = ls->b;
	double* x = ls->x;

	double** P = (double**) malloc( (kMax+1) * sizeof(double*));
	double** Q = (double**) malloc( (kMax+1) * sizeof(double*));
	for (int i = 0; i < kMax+1; i++) {
		P[i] = alocarVetor(n);
		Q[i] = alocarVetor(n);
	}
	double alpha, beta;
	double* Ap = alocarVetor(n);
	double* q = alocarVetor(n);
	double* residuo = alocarVetor(n);

	switch (precondicionamento) {
	case ILU:	// LU*r0 = b ->  U*r0 = q  e  L*q = b
		resolverSistemaL(preL, q, b);
		resolverSistemaU(preU, residuo, q);
		break;
	case SEIDEL:
		resolverSistemaTriangularInferior(A, q, b);
		resolverSistemaTriangularSuperior(A, residuo, q);
		break;
	case SEM_PRECOND:
		copiarValores(b, residuo, n);
		break;
	}

	double normaResiduo = normaEuclidiana(b, n); //rho
	double limiteNorma = tolerancia * normaResiduo;

	int numTotalIter = 0;

	int k, l = 0;
	copiarValores(residuo, P[0], n);

	while (normaResiduo > limiteNorma && l < lMax) {

		produtoMatVet(A, P[0], Ap); // Ap = A x P[0]

		switch (precondicionamento) {
		case ILU:	// LU*Q[0] = A*P[0] = Ap ->  U*Q[0] = q  e  L*q = Ap
			resolverSistemaL(preL, q, Ap);
			resolverSistemaU(preU, Q[0], q);
			break;
		case SEIDEL:
			resolverSistemaTriangularInferior(A, q, Ap);
			resolverSistemaTriangularSuperior(A, Q[0], q);
			break;
		case SEM_PRECOND:
			copiarValores(Ap, Q[0], n);
			break;
		}

		k = 0;
		while (normaResiduo >= limiteNorma && k < kMax) {
			alpha = produtoEscalarVetVet(P[k], residuo, n);
			alpha /= produtoEscalarVetVet(P[k], Q[k], n);

			somarComProdutoEscalarVet(x, alpha, P[k], n); // x = x + alpha*P[k]
			somarComProdutoEscalarVet(residuo, (-alpha), Q[k], n); // r = r - alpha*Q[k]

			copiarValores(residuo, P[k+1], n);
			produtoMatVet(A, P[k+1], Ap); // Ap = A*P[k+1]

			switch (precondicionamento) {
			case ILU:	// LU*Q[k+1] = A*P[k+1] = Ap ->  U*Q[k+1] = q  e  L*q = Ap
				resolverSistemaL(preL, q, Ap);
				resolverSistemaU(preU, Q[k+1], q);
				break;
			case SEIDEL:
				resolverSistemaTriangularInferior(A, q, Ap);
				resolverSistemaTriangularSuperior(A, Q[k+1], q);
				break;
			case SEM_PRECOND:
				copiarValores(Ap, Q[k+1], n);
				break;
			}

			for (int j = 0; j <= k; j++) {
				beta = - produtoEscalarVetVet(P[j], Q[k+1], n);
				beta /= produtoEscalarVetVet(P[j], Q[j], n);

				somarComProdutoEscalarVet(P[k+1], beta, P[j], n); // P[k+1] = P[k+1] + beta * P[j]
				somarComProdutoEscalarVet(Q[k+1], beta, Q[j], n); // Q[k+1] = Q[k+1] + beta * Q[j]
			}

			normaResiduo = normaEuclidiana(residuo, n);

			//printf("residuo: %0.8lf\n", normaResiduo);
			if ((numTotalIter) % 10 == 0) {
				fprintf(arquivo, "%d %0.8lf %0.12lf\n", numTotalIter, log(normaResiduo), normaResiduo);
			}
			numTotalIter++;
			k++;
		}
		copiarValores(residuo, P[0], n);
		l++;
	}

	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	fprintf(arquivo, "Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter );
	printf("Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter);
	fclose(arquivo);

	freeVetor(residuo);
	// Clean P and Q
	for (int i = 0; i < kMax+1; i++) {
		freeVetor(P[i]);
		freeVetor(Q[i]);
	}
	FREE(P);
	FREE(Q);
	FREE(q);
	FREE(Ap);
}











/*----------------------------------------------------------------------------
 * METODOS SEM PRECONDICIONAMENTO
 *--------------------------------------------------------------------------*/

/*
void GMRES_Restart(LinearSystem* ls, int iMax, int lMax, double tolerancia, char* nomeMatriz) {
	clock_t start = clock();
	char nomeArquivo[100];
	sprintf(nomeArquivo, "./plot/%s_gmres_%d.dat", nomeMatriz, iMax);
	printf("salvando em: %s\n", nomeArquivo);
	FILE* arquivo = fopen(nomeArquivo, "w");
	if (arquivo == NULL) {
		ERROR("Nao foi possivel criar arquivo de saida.");
		exit(0);
	}


	printf("Resolvendo pelo GMRES...\n\n");
	MAT* A = ls->A;
	int n = A->n;
	double* x = ls->x;
	double* b = ls->b;
	// U(n x iMax+1), um vetor de tam n para cada interacao +1
	double** U = (double**) malloc( (iMax + 1) * sizeof(double*));
	for (int k = 0; k < iMax + 1; k++) {
		U[k] = alocarVetor(n);
	}
	// H(iMax x iMax+1), um vetor de tam iMax+1 para cada iteracao
	double** H = (double**) malloc( iMax * sizeof(double*));
	for (int k = 0; k < iMax; k++) {
		H[k] = alocarVetor(iMax+1);
	}

	double* y = alocarVetor(iMax);
	double* c = alocarVetor(iMax);
	double* s = alocarVetor(iMax);
	double* e = alocarVetor(iMax+1);

	double* residuo = copiarVetor(b,n);
	double* vetorUnitario = gerarVetorUnitario(n);
	double normaResiduo;


	double rho, epsilon = tolerancia * normaEuclidiana(b, n);
	int i, l = 0,  numTotalIter = 0;

	normaResiduo = normaEuclidiana(residuo,n);
	fprintf(arquivo, "%d\t%0.8lf\t%0.12lf\n", numTotalIter, log(normaResiduo), normaResiduo);

	do {
			//printf("l = %d\n", l);
			produtoMatVet(A, x, U[0]);
			subtrairVetDeVet(b, U[0], U[0], n);

			e[0] = normaEuclidiana(U[0], n);
			dividirVetorPorConstante(U[0], e[0], n);
			rho = e[0];

			i = 0;
			while (rho > epsilon && i < iMax) {
				produtoMatVet(A, U[i], U[i+1]); // U[i+1] = A x U[i]

				// Gram - Schmidt
				for (int j = 0; j <= i; j++) {
					H[i][j] = produtoEscalarVetVet(U[i+1], U[j], n); // Iteracao elem j do H atual
					somarComProdutoEscalarVet(U[i+1], -H[i][j], U[j], n);
				}

				H[i][i+1] = normaEuclidiana(U[i+1], n);
				dividirVetorPorConstante(U[i+1], H[i][i+1], n);

				//Algoritmo QR
				for (int j = 0; j < i; j++) {
					double temp = H[i][j];
					H[i][j] =  c[j] * temp + s[j] * H[i][j+1];
					H[i][j+1] = (-s[j]) * temp + c[j] * H[i][j+1];
				}
				double r = sqrt( pow(H[i][i], 2) + pow(H[i][i+1], 2));

				c[i] = H[i][i] / r;
				s[i] = H[i][i+1] / r;
				H[i][i] = r;
				H[i][i+1] = 0.0;

				e[i+1] = (-s[i]) * e[i];
				e[i] = c[i] * e[i];

				rho = fabs(e[i+1]);

				//printf("rho %d = %0.9lf\n", numTotalIter, rho);
				if (numTotalIter % 100 == 0) {
					fprintf(arquivo, "%d\t%0.8lf\t%0.12lf\n", numTotalIter, log(rho), rho);
				}

				numTotalIter++;
				i++;
			}

			i = i - 1;
			for (int j = i; j >= 0; j--) {
				double sum = 0.0;
				for (int l = j+1; l <= i; l++) {
					 sum += H[l][j] * y[l];
				}
				y[j] = (e[j] - sum) / H[j][j];
			}

			//printf("Computando x ...\n");
			for (int j = 0; j <= i; j++) {
				somarComProdutoEscalarVet(x, y[j], U[j], n);
			}

			//subtrairVetDeVet(vetorUnitario, x, residuo, n);
			//normaResiduo = normaEuclidiana(residuo, n);
			//fprintf(arquivo, "%d\t%0.8lf\t%0.12lf\n", numTotalIter, log(normaResiduo), normaResiduo);

			l++;
	} while (rho > epsilon && l < lMax);

	printf("Resolvido em %d iteracoes.\n", numTotalIter);
	// Limpa memoria
	for (int k = 0; k < iMax+1; k++) {
		freeVetor(U[k]);
	}
	FREE(U);
	for (int k = 0; k < iMax; k++) {
		freeVetor(H[k]);
	}
	FREE(H);
	freeVetor(residuo);
	freeVetor(vetorUnitario);
	freeVetor(y);
	freeVetor(c);
	freeVetor(s);
	freeVetor(e);

	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	fprintf(arquivo, "Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter );
	printf("Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter);
	fclose(arquivo);
}

void GMRES(LinearSystem* ls, int iMax, double tolerancia) {

	printf("Resolvendo pelo GMRES...\n\n");

	MAT* A = ls->A;
	int n = A->n;
	double* x = ls->x;
	double* b = ls->b;

	// U(n x iMax+1), um vetor de tam n para cada interacao +1
	double** U = (double**) malloc( (iMax + 1) * sizeof(double*));
	U[0] = copiarVetor(b, n);
	for (int k = 1; k < iMax + 1; k++) {
		U[k] = alocarVetor(n);
	}
	// H(iMax x iMax+1), um vetor de tam iMax+1 para cada iteracao
	double** H = (double**) malloc( iMax * sizeof(double*));
	for (int k = 0; k < iMax; k++) {
		H[k] = alocarVetor(iMax+1);
	}

	double* c = alocarVetor(iMax);
	double* s = alocarVetor(iMax);

	double* e = alocarVetor(iMax+1);
	e[0] = normaEuclidiana(U[0], n);
	dividirVetorPorConstante(U[0], e[0], n);

	double rho = e[0];
	double epsilon = tolerancia * normaEuclidiana(b, n);
	printf("epsilon = %lf\n",epsilon);

	int i = 0;
	while (rho > epsilon && i < iMax) {
		//printf("----%d----\n", i);
		produtoMatVet(A, U[i], U[i+1]); // U[i+1] = A x U[i]

		// Gram - Schmidt

		//  < OU <= ?????????????
		for (int j = 0; j <= i; j++) {

			H[i][j] = produtoEscalarVetVet(U[i+1], U[j], n); // Iteracao elem j do H atual
			somarComProdutoEscalarVet(U[i+1], -H[i][j], U[j], n);
		}

		H[i][i+1] = normaEuclidiana(U[i+1], n);
		dividirVetorPorConstante(U[i+1], H[i][i+1], n);

		//Algoritmo QR
		for (int j = 0; j < i; j++) {
			double temp = H[i][j];
			H[i][j] =  c[j] * temp + s[j] * H[i][j+1];
			H[i][j+1] = (-s[j]) * temp + c[j] * H[i][j+1];
		}
		//printMatrix(H, iMax, iMax+1, "H");
		//printMatrix(U, iMax+1, n, "U");

		double r = sqrt( pow(H[i][i], 2) + pow(H[i][i+1], 2));

		c[i] = H[i][i] / r;
		s[i] = H[i][i+1] / r;
		H[i][i] = r;
		H[i][i+1] = 0.0;

		e[i+1] = (-s[i]) * e[i];
		e[i] = c[i] * e[i];

		rho = fabs(e[i+1]);
		printf("rho %d = %lf\n", i, rho);
		i++;
	}

	i = i - 1;
	//printVector(e,"e");
	double* y = alocarVetor(iMax);
	for (int j = i; j >= 0; j--) {
		double sum = 0.0;
		for (int l = j+1; l <= i; l++) {
			 sum += H[l][j] * y[l];
		}
		y[j] = (e[j] - sum) / H[j][j];
	}

	printf("Computando x ...\n");
	for (int j = 0; j <= i; j++) {
		somarComProdutoEscalarVet(x, y[j], U[j], n);
	}

	// Limpa memoria
	for (int k = 0; k < iMax+1; k++) {
		freeVetor(U[k]);
	}
	FREE(U);
	for (int k = 0; k < iMax; k++) {
		freeVetor(H[k]);
	}
	FREE(H);

	freeVetor(c);
	freeVetor(s);
	freeVetor(e);
	freeVetor(y);
}

void LCD_Restart(LinearSystem* ls, int kMax, int lMax, double tolerancia, char* nomeMatriz) {
	char nomeArquivo[100];
	sprintf(nomeArquivo, "./plot/%s_lcd_%d.dat", nomeMatriz, kMax);
	printf("salvando em: %s\n", nomeArquivo);
	FILE* arquivo = fopen(nomeArquivo, "w");
	clock_t start = clock();

	printf("Resolvendo pelo LCD...\n\n");
	int n = ls->A->n;
	MAT* A = ls->A;
	double* b = ls->b;
	double* x = ls->x;

	double** P = (double**) malloc( (kMax+1) * sizeof(double*));
	double** Q = (double**) malloc( (kMax+1) * sizeof(double*));
	for (int i = 0; i < kMax+1; i++) {
		P[i] = alocarVetor(n);
		Q[i] = alocarVetor(n);
	}
	double* listaNormaResiduos = alocarVetor(kMax*lMax);
	double alpha, beta;

	double* residuo = copiarVetor(b,n); // r0 = b - Ax
	double normaResiduo = normaEuclidiana(b, n); //ro
	double epsilon = tolerancia * normaResiduo; //limite

	int numTotalIter = 0;

	int k, l = 0;
	copiarValores(residuo, P[0], n);

	while (normaResiduo > epsilon && l < lMax) {
		//printf("l = %d\n", l);
		produtoMatVet(A, P[0], Q[0]); // Q[0] = A x P[0]

		k = 0;
		while (normaResiduo >= epsilon && k < kMax) {
			alpha = produtoEscalarVetVet(P[k], residuo, n);
			alpha /= produtoEscalarVetVet(P[k], Q[k], n);

			somarComProdutoEscalarVet(x, alpha, P[k], n); // x = x + alpha*P[k]
			somarComProdutoEscalarVet(residuo, (-alpha), Q[k], n); // r = r - alpha*Q[k]

			copiarValores(residuo, P[k+1], n);
			produtoMatVet(A, P[k+1], Q[k+1]);

			for (int j = 0; j <= k; j++) {
				beta = - produtoEscalarVetVet(P[j], Q[k+1], n);
				beta /= produtoEscalarVetVet(P[j], Q[j], n);

				somarComProdutoEscalarVet(P[k+1], beta, P[j], n); // P[k+1] = P[k+1] + beta * P[j]
				somarComProdutoEscalarVet(Q[k+1], beta, Q[j], n); // Q[k+1] = Q[k+1] + beta * Q[j]
			}

			normaResiduo = normaEuclidiana(residuo, n);

			//printf("residuo: %0.8lf\n", normaResiduo);
			if ((numTotalIter) % 100 == 0) {
				fprintf(arquivo, "%d %0.8lf %0.12lf\n", numTotalIter, log(normaResiduo), normaResiduo);
			}
			numTotalIter++;
			k++;
		}
		copiarValores(residuo, P[0], n);
		l++;
	}

	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	fprintf(arquivo, "Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter );
	printf("Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter);
	fclose(arquivo);

	freeVetor(residuo);
	// Clean P and Q
	for (int i = 0; i < kMax+1; i++) {
		freeVetor(P[i]);
		freeVetor(Q[i]);
	}
	FREE(P);
	FREE(Q);

	//for (int i = 0; i < numTotalIter; i++)
	//	printf("r(%d)\t= %lf\n", i, listaNormaResiduos[i]);
	FREE(listaNormaResiduos);
}


void LCD(LinearSystem* ls, int kMax, double tolerancia) {

	printf("Resolvendo pelo LCD...\n\n");
	int n = ls->A->n;
	MAT* A = ls->A;
	double* b = ls->b;
	double* x = ls->x;
	double** P = (double**) malloc( (kMax+1) * sizeof(double*));
	double** Q = (double**) malloc( (kMax+1) * sizeof(double*));
	for (int i = 0; i < kMax+1; i++) {
		P[i] = alocarVetor(n);
		Q[i] = alocarVetor(n);
	}
	double* r = copiarVetor(b,n); // r0 = b - Ax
	double normaResiduo = normaEuclidiana(r, n);
	double alpha, beta, limiteResiduo = tolerancia * normaResiduo;

	copiarValores(r, P[0], n); // P para Pt*A*P != 0
	produtoMatVet(A, P[0], Q[0]); // Q = AP

	int k = 0;
	while (normaResiduo > limiteResiduo && k < kMax) {

		alpha = produtoEscalarVetVet(P[k], r, n);
		alpha /= produtoEscalarVetVet(P[k], Q[k], n);

		somarComProdutoEscalarVet(x, alpha, P[k], n);
		somarComProdutoEscalarVet(r, (-alpha), Q[k], n);

		copiarValores(r, P[k+1], n);
		produtoMatVet(A, P[k+1], Q[k+1]);

		for (int i = 0; i <= k; i++) {
			beta = - produtoEscalarVetVet(P[i], Q[k+1], n);
			beta /= produtoEscalarVetVet(P[i], Q[i], n);

			somarComProdutoEscalarVet(P[k+1], beta, P[i], n);
			somarComProdutoEscalarVet(Q[k+1], beta, Q[i], n);
		}

		normaResiduo = normaEuclidiana(r, n);
		printf("rho %d = %lf\n", k, normaResiduo);

		k++;
	}

	printf("Resolvido em %d iteracoes.\n", k);
	freeVetor(r);
	// Clean P and Q
	for (int i = 0; i < kMax+1; i++) {
		freeVetor(P[i]);
		freeVetor(Q[i]);
	}
	FREE(P);
	FREE(Q);
}
*/

