#include <stdio.h>
#include "csrMatrixSystem.h"
#include "vector.h"
#include "util.h"


LinearSystem* criarSistemaComSolUnitaria(MAT* A) {
	LinearSystem* ls = (LinearSystem*) malloc (sizeof(LinearSystem));
	ls->A = A;
	double* b = alocarVetor(A->m);

	int i = 0, k = 0;
	while (k < A->nz) {
		int indiceProxLinha = A->IA[i+1];
		//k = A->IA[i];
		double soma = 0.0;
		while (k < indiceProxLinha) {
			soma += A->AA[k];
			k++;
		}
		b[i] = soma;
		i++;
	}

	ls->b = b;
	double* x = alocarVetor(A->m);
	ls->x = x;
	return ls;
}

void freeLinearSystem(LinearSystem *ls) {
	MATRIX_clean(ls->A);
	FREE(ls->b);
	FREE(ls->x);
	FREE(ls);
}

void printLinearSystem(LinearSystem *ls) {
	MATRIX_print(ls->A,"A");
	printVetor(ls->b, ls->A->m, "b");
	printVetor(ls->x, ls->A->m, "x");
}

// A x v
void produtoMatVet(MAT* A, double* v, double* resultado) {
	int valoresNaoNulos = A->nz;
	int i = 0, k = 0;
	while (k < valoresNaoNulos) {
		int indiceProxLinha = A->IA[i+1];
		double soma = 0.0;
		while (k < indiceProxLinha) {
			int j = A->JA[k];
			soma += A->AA[k] * v[j];
			k++;
		}
		resultado[i] = soma;
		i++;
	}
}


int isIgual(double a, double b) {
	return (fabs(a - b) < 0.000001);
}

void resolverSistemaL(MAT* L, double* x, double* b) {
	int i, j, k, indiceLinhaAtual, indiceProxLinha;
	int n = L->n;
	double soma, diagonal;

	for (i = 0; i < n; i++)
		x[i] = 0.0;

	for (i = 0; i < n; i++) {
		indiceLinhaAtual = L->IA[i];
		k = indiceLinhaAtual;
		indiceProxLinha = L->IA[i+1];
		soma = 0.0;
		diagonal = L->D[i];
		while (k < indiceProxLinha) {
			soma += L->AA[k] * x[ L->JA[k] ];
			k++;
		}

		if (isIgual(diagonal, 0.0)) {
			printf("Diagonal == 0\n");
			exit(1);
		}
		else {
			x[i] = b[i] - soma;
			x[i] /= diagonal;
		}
	}
}


void resolverSistemaU(MAT* U, double* x, double* b) {
	int i, j, k, indiceLinhaAtual, indiceProxLinha;
	int n = U->n;
	double soma, diagonal;

	for (i = n-1; i >= 0; i--) {
		indiceLinhaAtual = U->IA[i];
		k = indiceLinhaAtual;
		indiceProxLinha = U->IA[i+1];
		soma = 0.0;
		diagonal = U->D[i];
		while (k < indiceProxLinha) {
			soma += U->AA[k] * x[ U->JA[k] ];
			k++;
		}

		if (isIgual(diagonal, 0.0)) {
			printf("Diagonal == 0\n");
			exit(1);
		}
		else {
			x[i] = b[i] - soma;
			x[i] /= diagonal;
		}
	}
}


void resolverSistemaTriangularInferior(MAT* L, double* x, double* b) {
	int i, j, k, indiceLinhaAtual, indiceProxLinha;
	int n = L->n;
	double soma, diagonal;

	for (i = 0; i < n; i++)
		x[i] = 0.0;

	for (i = 0; i < n; i++) {
		//printf("=> %d:\n", i);

		indiceLinhaAtual = L->IA[i];
		k = indiceLinhaAtual;
		indiceProxLinha = L->IA[i+1];
		soma = 0.0;
		diagonal = L->D[i];
		while (k < indiceProxLinha && L->JA[k] < i) {
			//printf(" soma += %lf * %lf\n", L->AA[k], x[ L->JA[k] ]);
			soma += L->AA[k] * x[ L->JA[k] ];
			k++;
		}

		if (isIgual(diagonal, 0.0)) {
			printf("Diagonal == 0\n");
			exit(1);
		}
		else {
			//printf("x[%d] = %lf - %lf / %lf\n", i, b[i], soma, diagonal);
			x[i] = b[i] - soma;
			x[i] /= diagonal;
			//printf("x[%d] = %lf\n", i, x[i]);
		}
	}

	/*printf("X = \n");
	for (int i = 0; i < n; i++)
		printf("%lf\n", x[i]);
		*/
}


void resolverSistemaTriangularSuperior(MAT* U, double* x, double* b) {
	int i, j, k, indiceLinhaAtual, indiceProxLinha;
	int n = U->n;
	double soma, diagonal;

	for (i = n-1; i >= 0; i--) {
		//printf("=> %d:\n", i);

		indiceLinhaAtual = U->IA[i];
		//printf("atual: %d\n", indiceLinhaAtual);
		k = indiceLinhaAtual;
		indiceProxLinha = U->IA[i+1];
		//printf("atual: %d\n", indiceProxLinha);
		soma = 0.0;
		diagonal = U->D[i];
		while (k < indiceProxLinha) {
			if (U->JA[k] > i) {
				//printf(" soma += %lf * %lf\n", U->AA[k], x[ U->JA[k] ]);
				soma += U->AA[k] * x[ U->JA[k] ];
			}
			k++;
		}

		if (isIgual(diagonal, 0.0)) {
			printf("Diagonal == 0\n");
			exit(1);
		}
		else {
			//printf("x[%d] = %lf - %lf / %lf\n", i, b[i], soma, diagonal);
			x[i] = b[i] - soma;
			x[i] /= diagonal;
			//printf("x[%d] = %lf\n", i, x[i]);
		}
	}

	/*printf("X = \n");
	for (int i = 0; i < n; i++)
		printf("%lf\n", x[i]);
*/
}



/*
// result = v1 + v2 . v3
void getSumdoubleWithProductdoubledouble (double* result, double* v1, double* v2, double* v3) {
	int n = v1->size;
	for (int i = 0; i < n; i++) {
		result->val[i] =  v1->val[i] + (v2->val[i] * v3->val[i]);
	}
}

// v(t) x M x v
double getCrossProductTrasposeddoubleMatrixdouble(double* v, MAT* M) {
	double* productMv = allocatedouble(v->size);
	computeProductMatrixdouble(productMv, M, v);
	double result = getDotProductdoubles(v,productMv);
	freedouble(productMv);
	return result;
}
*/


